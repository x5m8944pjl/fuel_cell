# -*- coding: UTF-8 –*-
"""
author:zqguo
time:2019-11-04 09:56

"""
import sys


def get_linux_version():
    print("system version...".format(sys.version))


def get_mem_info():
    mem_info = ""
    f_mem_info = open("/proc/meminfo")
    try:
        for line in f_mem_info:
            if line.find("MemTotal") == 0:
                mem_info += line.strip() + ", "
            elif line.find("SwapTotal") == 0:
                mem_info += line.strip()
                break
        print("mem_info---- {:s}".format(mem_info))
    finally:
        f_mem_info.close()


if __name__ == '__main__':
    get_mem_info()
