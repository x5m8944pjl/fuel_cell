# -*- coding: UTF-8 –*-
"""
author:zqguo
time:2019-11-05 09:32

"""
import re
import subprocess

if __name__ == '__main__':
    try:
        # schtasks /Query /TN "NvProfileUpdaterDaily_{B2FE1952-0186-46C3-BAEC-A80AA35AC5B8}"
        out_bytes = subprocess.check_output(
            ['schtasks', '/Query', '/TN', 'NvProfileUpdaterDaily_{B2FE1952-0186-46C3-BAEC-A80AA35AC5B8}'])
    except subprocess.CalledProcessError as e:
        out_bytes = e.output  # Output generated before error
        # code = e.returncode  # Return code
    res = out_bytes.decode('GBK')
    # print(res)
    # print(type(res))
    match = re.search(r'\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}', res)
    print(match.group())
