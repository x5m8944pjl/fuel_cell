# -*- coding: UTF-8 –*-
"""
author:zqguo
time:2019-11-04 09:44

"""
import os
import time


def ts_to_time(timestamp):
    time_struct = time.localtime(timestamp)
    return time.strftime('%Y-%m-%d %H:%M:%S', time_struct)


# 获取文件大小
def get_file_size(filepath):
    filepath = unicode(filepath, 'utf8')
    fsize = os.path.getsize(filepath)
    return fsize


def get_file_modify_time(filepath):
    filepath = unicode(filepath, 'utf8')
    t = os.path.getmtime(filepath)
    return ts_to_time(t)


if __name__ == '__main__':
    fsize = get_file_size("D:/code/backend/python/fuel_cell/fuel_cell_app/tests/Demo01.py")
    print(fsize)
    t = get_file_modify_time("D:/code/backend/python/fuel_cell/fuel_cell_app/tests/Demo01.py")
    print(t)
