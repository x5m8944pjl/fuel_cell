"""
author:zqguo
time:2019-11-05 15:57

"""
from datetime import datetime
import ftplib
from dateutil import parser

if __name__ == '__main__':
    # datetime.utcfromtimestamp()
    # datetime.fromtimestamp()
    # ftp = ftplib.FTP("192.168.0.65")
    # ftp.login("ZP300", "zp791")
    #
    # ftp.nlst('/')
    # ftp.size()
    # res = parser.parse('20191104014032', ignoretz=False)
    # print(res)
    # print(type(res))
    # res = datetime.utcfromtimestamp(20191104014032)
    # print(res)
    res = datetime.strptime('20191104014032', '%Y%m%d%H%M%S')
    print(res)
    """
    >>> from datetime import datetime
    >>> res = datetime.strptime('20191104014032', '%Y%m%d%H%M%S')
    >>> res
    datetime.datetime(2019, 11, 4, 1, 40, 32)
    >>> datetime.utcfromtimestamp(res)
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    TypeError: a float is required
    >>> datetime.utcfromtimestamp()
    KeyboardInterrupt
    >>> import time
    >>> time.mktime(res.timetuple())
    1572831632.0

    """
    print(type(res))
    print()


