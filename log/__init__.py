# coding=utf-8

from log import Log

logger_dict = {}


def get_logger(name, config_file='logging.conf'):
    return Log(name, config_file).logging_init()
