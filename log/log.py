from logging import config, getLogger


class Log:

    def __init__(self, logger, logging_conf='logging.conf'):
        self.logging_conf = logging_conf
        self.logger = logger

    def logging_init(self):
        print('Using logger config file:', self.logging_conf)
        config.fileConfig(self.logging_conf)
        return getLogger(self.logger)
