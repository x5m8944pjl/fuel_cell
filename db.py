# coding=utf-8
"""
author:zqguo
time:2019-08-09 15:31
sqlite operate
"""
import time

import dataset

from log import get_logger

LOGGER = get_logger('fuel_cell_app', 'logging.conf')

DB_URL = 'sqlite:///alarm.db'


def init_db():
    engine_kwargs = {
        'connect_args': {"check_same_thread": False}
    }
    db = dataset.connect(DB_URL, engine_kwargs=engine_kwargs)
    return db


def insert_alarm(msg):
    try:
        LOGGER.info('start save alarm: {} to sqlite'.format(msg))
        current_ts = int(time.time())
        db = init_db()
        with db as d:
            table = d['alarm']
            table.insert(dict(msg=msg, ts=current_ts, status=1))
        LOGGER.info('save alarm: {} to sqlite is success'.format(msg))
    except Exception as e:
        LOGGER.exception(e)


def update_alarm_status(id_):
    try:
        LOGGER.info('start update alarm')
        current_ts = int(time.time())
        db = init_db()
        with db as d:
            alarm = d['alarm']
            data = dict(status=3, ts=current_ts, id=id_)
            alarm.update(data, ['id'])
        LOGGER.info('update alarm is success')
    except Exception as e:
        LOGGER.exception(e)


def find_latest_alarm():
    try:
        LOGGER.info('start find latest alarm')
        db = init_db()
        with db as d:
            alarm = d['alarm']
            res = alarm.find_one(order_by='-ts')
            LOGGER.info('find latest alarm is success')
            return dict(res) if res else None
    except Exception as e:
        LOGGER.exception(e)
        return None
    # conn = sqlite3.connect('alarm.db')
    # c = conn.cursor()
    # cursor = c.execute("SELECT id, msg, ts, status  from alarm where id={}".format(id_))
    # for row in cursor:
    #     print("id = ", row[0])
    #     print("msg = ", row[1])
    #     print("ts = ", row[2])
    #     print("status = ", row[3])


if __name__ == '__main__':
    insert_alarm('F_A_1#:31,31,0')
    # update_alarm_status('F_A_1#:31,31,0')
    res = find_latest_alarm()
    print(res)
    # update_alarm_status(1)
    # create_table()
    # insert_alarm('F_A_1#:31,31,0')
    # select_alarm(1)
    # db = dataset.connect('sqlite:///mydatabase.db')
    # with db as d:
    #     table = d['user']
    #     table.update(dict(name='aavvvvvvvvvvvv', age=46, country='ss', id=8), ['id'])
    # # db.commit()
    # users = db['user'].all()
    # print([dict(user) for user in users])
    #
    # users = db['user'].find_one(order_by='-id')
    # print(dict(users))
