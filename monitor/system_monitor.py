# -*- coding: UTF-8 –*-
"""
author:zqguo
time:2019-11-04 10:22

"""
import ftplib
import os
import re
import subprocess
import time
import json
from datetime import datetime

import psutil

from utils import get_config

cfg = get_config('cfg.json')


def ping_servers():
    ping_servers = cfg['ping_servers']
    print(ping_servers)
    res = {}
    for k, ip in ping_servers.items():
        if subprocess.call(["ping", "-c", "2", ip]) == 0:
            res[k] = 1
        else:
            res[k] = 0
    return res


def process_info():
    process_name = cfg['monitor_process']
    psutil_names = process_name['psutil']
    supervisor_names = process_name['supervisor']
    process_status = {
        "RecCOMV1.1": 0,
        "fuel_cell_monitor": 0,
        "vsftpd": 0,
        "beaconCtr": 0,
        "filemon": 0,
        "ssh_tunnel_9100": 0
    }
    for p in psutil.process_iter():
        pstr = str(p)
        for pname in psutil_names:
            f = re.compile(pname, re.I)
            if f.search(pstr):
                process_status[pname] = 1

    for supervisor_name in supervisor_names:
        try:
            out_bytes = subprocess.check_output(['supervisorctl', 'status', supervisor_name])
        except subprocess.CalledProcessError as e:
            out_bytes = e.output  # Output generated before error
            # code = e.returncode  # Return code
        print(out_bytes.decode('utf-8'))
        res = out_bytes.decode('utf-8')
        if 'RUNNING' in res:
            process_status[supervisor_name] = 1
    return process_status


def time_str_to_timestamp(date_str):
    try:
        t = datetime.strptime(date_str, '%Y-%m-%d').timetuple()
        res = int(time.mktime(t))
        return res
    except Exception as e:
        print(e)
        return 0


def timestamp_to_date(t):
    time_local = time.localtime(t)
    dt = time.strftime("%Y_%m_%d", time_local)
    return dt


# 获取文件大小
def get_file_size(filepath):
    fsize = os.path.getsize(filepath)
    return round(fsize / 1024, 2)


def get_file_modify_time(filepath):
    t = os.path.getmtime(filepath)
    return int(t)


def file_monitor():
    file_monitor = cfg['file_monitor']
    rv = {}
    for type_, fpath in file_monitor.items():
        date_list = set(
            ['{}-{}-{}'.format(str(f)[-14:-10:], str(f)[-9:-7:], str(f)[-6:-4:]) for f in os.listdir(fpath)])
        if type_ == "motion":
            timestamp = [time_str_to_timestamp(date_) for date_ in date_list]
            max_timestamp = max(timestamp)
            latest_date = timestamp_to_date(max_timestamp)
            latest_date_filename = 'COM1_{}.txt'.format(latest_date)
            latest_date_filepath = os.path.join(fpath, latest_date_filename)
        elif type_ == "trimble":
            timestamp = [time_str_to_timestamp(date_) for date_ in date_list]
            max_timestamp = max(timestamp)
            latest_date = timestamp_to_date(max_timestamp)
            latest_date_filename = 'PORT3001_{}.txt'.format(latest_date)
            latest_date_filepath = os.path.join(fpath, latest_date_filename)
        elif type_ == "weather":
            timestamp = [time_str_to_timestamp(date_) for date_ in date_list]
            max_timestamp = max(timestamp)
            latest_date = timestamp_to_date(max_timestamp)
            latest_date_filename = 'COM2_{}.txt'.format(latest_date)
            latest_date_filepath = os.path.join(fpath, latest_date_filename)

        latest_date_filesize = get_file_size(latest_date_filepath)
        latest_date_modify_time = get_file_modify_time(latest_date_filepath)
        rv.update({
            type_: [latest_date_modify_time, latest_date_filesize]
        })
    return rv


def mem_and_dis_monitor():
    mem = psutil.virtual_memory()
    available = mem.available
    disk_info = psutil.disk_usage('/')
    free = disk_info.free
    return {
        'mem_available': '{}G'.format(available / (1024 * 1024 * 1024)),
        'disk_free': '{}G'.format(free / (1024 * 1024 * 1024))
    }


def ftp_lidar_monitor():
    ftp = ftplib.FTP("192.168.0.65")
    ftp.login("ZP300", "zp791")
    flist = ftp.nlst()

    wind10_date_list = set(
        ['{}-{}-{}'.format(f[-20:-16], f[-14:-12], f[-10:-8]) for f in flist if 'Wind10_' in f and 'zip' in f]
    )

    wind_date_list = set(
        ['{}-{}-{}'.format(f[-20:-16], f[-14:-12], f[-10:-8]) for f in flist if 'Wind_' in f and 'zip' in f]
    )
    # print(wind10_date_list)
    # print(wind_date_list)
    wind10_timestamp = [time_str_to_timestamp(date_) for date_ in wind10_date_list]
    wind10_max_timestamp = max(wind10_timestamp)
    # print(wind10_max_timestamp)

    wind_timestamp = [time_str_to_timestamp(date_) for date_ in wind_date_list]
    wind_max_timestamp = max(wind_timestamp)
    # print(wind_max_timestamp)

    # Wind10_791@Y2019_M11_D04.ZPH.zip
    win10_latest_filename = time.strftime('Wind10_791@Y%Y_M%m_D%d.ZPH.zip', time.localtime(wind10_max_timestamp))
    # print(win10_latest_filename)

    win_latest_filename = time.strftime('Wind_791@Y%Y_M%m_D%d.ZPH.zip', time.localtime(wind_max_timestamp))
    # print(win_latest_filename)

    win10_latest_modify_ts = ftp.voidcmd("MDTM {}".format(win10_latest_filename))[4:].strip()
    # print(win10_latest_modify_ts)
    res = datetime.strptime(win10_latest_modify_ts, '%Y%m%d%H%M%S')
    win10_latest_modify_ts = int(time.mktime(res.timetuple()))
    # print(win10_latest_modify_ts)
    win10_latest_size = ftp.size(win10_latest_filename)

    win_latest_modify_ts = ftp.voidcmd("MDTM {}".format(win_latest_filename))[4:].strip()
    # print(win_latest_modify_ts)
    res = datetime.strptime(win_latest_modify_ts, '%Y%m%d%H%M%S')
    win_latest_modify_ts = int(time.mktime(res.timetuple()))
    # print(win_latest_modify_ts)
    win_latest_size = ftp.size(win_latest_filename)

    rv = {
        'wind10': [win10_latest_modify_ts, '{}K'.format(win10_latest_size / 1024)],
        'wind': [win_latest_modify_ts, '{}K'.format(win_latest_size / 1024)]
    }
    return rv


def monitor_data_pc():
    """
    monitor data pc
    :return:
    """
    status_servers = ping_servers()
    # print(status_servers)
    # ping
    process_status = process_info()
    # print(process_status)

    file_status = file_monitor()
    # print(file_status)

    lidar_status = ftp_lidar_monitor()

    mem_and_dis_status = mem_and_dis_monitor()
    # print(mem_and_dis_status)
    return status_servers, lidar_status, process_status, file_status, mem_and_dis_status


# monitor back pc
def monitor_backup_pc():
    """
    读取文件
    :return:
    """
    with open('/root/deploy/fuel_cell_app/monitor_data.json', 'r') as f:
        data = json.load(f)
        beidou_status = data['beidou']
        buoy_rsnc_motion_next_time = data['buoy_rsync_motion_data']
        buoy_rsnc_lidar_next_time = data['buoy_rsync_lidar_data']
        buoy_corr_data_next_time = data['buoy_corr_lidar_data']
        lastest_motion_ts = data['motion'][0]
        lastest_motion_filesize = data['motion'][1]
        lastest_trimble_ts = data['trimble'][0]
        lastest_trimble_filesize = data['trimble'][1]
        lastest_weather_ts = data['weather'][0]
        lastest_weather_filesize = data['weather'][1]
        lastest_corr_ts = data['corr_file_dir'][0]
        lastest_corr_filesize = data['corr_file_dir'][1]
        lastest_lidar10_ts = data['win10'][0]
        lastest_lidar10_filesize = data['win10'][1]
        lastest_lidar_ts = data['win'][0]
        lastest_lidar_filesize = data['win'][1]
        mem_aval = data['mem_aval']
        c_free = data['c_free']
        d_free = data['d_free']
        return {
            'beidou_status': beidou_status,
            'buoy_rsnc_motion_next_time': buoy_rsnc_motion_next_time,
            'buoy_rsnc_lidar_next_time': buoy_rsnc_lidar_next_time,
            'buoy_corr_data_next_time': buoy_corr_data_next_time,
            'lastest_motion_ts': lastest_motion_ts,
            'lastest_motion_filesize': '{}K'.format(lastest_motion_filesize),
            'lastest_trimble_ts': lastest_trimble_ts,
            'lastest_trimble_filesize': '{}K'.format(lastest_trimble_filesize),
            'lastest_weather_ts': lastest_weather_ts,
            'lastest_weather_filesize': '{}K'.format(lastest_weather_filesize),
            'lastest_corr_ts': lastest_corr_ts,
            'lastest_corr_filesize': '{}K'.format(lastest_corr_filesize),
            'lastest_lidar10_ts': lastest_lidar10_ts,
            'lastest_lidar10_filesize': '{}K'.format(lastest_lidar10_filesize),
            'lastest_lidar_ts': lastest_lidar_ts,
            'lastest_lidar_filesize': '{}K'.format(lastest_lidar_filesize),
            'mem_aval': '{}G'.format(mem_aval),
            'c_free': '{}G'.format(c_free),
            'd_free': '{}G'.format(d_free)
        }


if __name__ == '__main__':
    monitor_data_pc()
    res = monitor_backup_pc()
    print(res)
