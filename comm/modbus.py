# -*- coding: UTF-8 –*-
"""
author:zqguo
time:2019-07-23 16:08

"""

import modbus_tk.defines as cst
import modbus_tk.modbus_rtu as modbus_rtu
import serial

from log import get_logger

LOGGER = get_logger('fuel_cell_app', 'logging.conf')


class ModBus(object):

    def __init__(self, config):
        self.port = config['port']
        self.slave = config['slave_addr']
        self.baudrate = config['baudrate']
        self.master = None
        self.connect()

    def __str__(self):
        return 'Device port(%s)' % self.port

    def connect(self):
        LOGGER.info('MBConnecting to (%s)' % self.port)
        self.master = modbus_rtu.RtuMaster(serial.Serial(port=self.port,
                                                         baudrate=self.baudrate, bytesize=8,
                                                         parity='N', stopbits=1, xonxoff=0))
        self.master.set_timeout(5.0)
        self.master.set_verbose(True)
        self.master.open()

    def disconnect(self):
        self.master.close()
        self.master = None

    def test_connection(self):
        return self.master.execute(self.slave, cst.READ_HOLDING_REGISTERS, 5, 1)

    def is_connected(self):
        try:
            self.test_connection()
        except Exception as e:
            LOGGER.error(e)
            return False
        return True

    def write_int32s_data(self, addr, f):
        LOGGER.debug('MBWriteFloat addr:%d,f:%f.', addr, f)
        try:
            self.master.execute(self.slave, cst.WRITE_MULTIPLE_REGISTERS,
                                starting_address=addr, output_value=[f])
        except Exception as e:
            LOGGER.exception(e)
            return {'ec': False, 'msg': str(e)}
        return {'ec': True, 'msg': 'OK'}

    def read_int32s_data(self, addr, cnt):
        return self.master.execute(
            self.slave, cst.READ_INPUT_REGISTERS, addr,
            quantity_of_x=cnt * 2, data_format='>l')

    def read_uint32s_data(self, addr, cnt):
        return self.master.execute(
            self.slave, cst.READ_INPUT_REGISTERS, addr,
            quantity_of_x=cnt * 2, data_format='>L')

    def write_coils(self, addr, ints):
        try:
            self.master.execute(
                self.slave, cst.WRITE_MULTIPLE_COILS,
                starting_address=addr,
                output_value=ints)
        except Exception as e:
            return {'ec': False, 'msg': str(e)}
        return {'ec': True, 'msg': 'OK'}

    def read_coils(self, addr, cnt):
        return self.master.execute(self.slave, cst.READ_COILS, addr, cnt)

    def write_int16s_data(self, addr, ints):
        self.master.execute(
            self.slave, cst.WRITE_MULTIPLE_REGISTERS,
            starting_address=addr,
            output_value=[ints])

    def read_uint16s_data(self, addr, cnt):
        return self.master.execute(
            self.slave, cst.READ_INPUT_REGISTERS, addr,
            quantity_of_x=cnt, data_format='>h')

    def read_int16s_data(self, addr, cnt):
        return self.master.execute(
            self.slave, cst.READ_INPUT_REGISTERS, addr,
            quantity_of_x=cnt, data_format='>h')

    def read_write_int16s_data(self, addr, ints):
        self.master.execute(
            self.slave, cst.READ_WRITE_MULTIPLE_REGISTERS,
            starting_address=addr,
            output_value=[ints])
