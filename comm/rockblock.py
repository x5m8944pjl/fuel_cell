# -*- coding: UTF-8 –*-
import glob
import sys
import time

import serial

from log import get_logger

LOGGER = get_logger('fuel_cell_app', 'logging.conf')


class RockBlockProtocol(object):

    def connected(self): pass

    def disconnected(self): pass

    # SIGNAL
    def signal_update(self, signal): pass

    def signal_pass(self): pass

    def signal_fail(self): pass

    # MT
    def rx_started(self): pass

    def rx_failed(self): pass

    def rx_received(self, mt_msn, data): pass

    def rx_message_queue(self, count): pass

    # MO
    def tx_started(self): pass

    def tx_failed(self): pass

    def tx_success(self, mo_msn): pass


class RockBlockException(Exception):
    pass


class RockBlock(object):
    IRIDIUM_EPOCH = 1399818235000  # May 11, 2014, at 14:23:55 (This will be 're-epoched' every couple of years!)

    def __init__(self, callback, rbs, read_line_q, sbd_q):

        self.callback = callback
        self.autoSession = True
        self.read_line_q = read_line_q
        self.sbd_q = sbd_q

        try:
            self.s = rbs
            if self.ping():
                if self.callback is not None and callable(self.callback.connected):
                    self.callback.connected()
                    return
        except Exception as e:
            LOGGER.exception(e)

    # Ensure that the connection is still alive
    def ping(self):
        self.ensure_connection_status()

        command = 'AT'

        self.s.write(command + '\r')

        if self.s.readline().strip() == command:
            if self.s.readline().strip() == 'OK':
                return True
        return False

    # Handy function to check the connection is still alive, else throw an Exception
    def ping_exception(self):
        self.ensure_connection_status()

        self.s.timeout = 5
        if not self.ping():
            raise RockBlockException

        self.s.timeout = 60

    def request_signal_strength(self):
        self.ensure_connection_status()

        command = 'AT+CSQ'

        self.s.write(command + '\r')
        LOGGER.info('write: {} to read_line_q'.format(command))
        time.sleep(3)

        send_cmd = self.read_line_q.get(timeout=30)
        LOGGER.info('sen_cmd: {}'.format(send_cmd))

        if send_cmd == command:

            response = self.read_line_q.get(timeout=30)

            if response.find('+CSQ') >= 0:
                self.read_line_q.get(timeout=30)
                if len(response) == 6:
                    return int(response[5])
        return -1

    def message_check(self):
        self.ensure_connection_status()

        if self.callback and callable(self.callback.rx_started):
            self.callback.rx_started()

        if self.attempt_connection() and self.attempt_session():
            return True
        else:
            if self.callback and callable(self.callback.rx_failed):
                self.callback.rx_failed()

    def network_time(self):
        self.ensure_connection_status()

        command = 'AT-MSSTM'

        self.s.write(command + '\r')

        if self.s.readline().strip() == command:

            response = self.s.readline().strip()

            self.s.readline().strip()  # BLANK
            self.s.readline().strip()  # OK

            if 'no network service' not in response:
                utc = int(response[8:], 16)
                utc = int((self.IRIDIUM_EPOCH + (utc * 90)) / 1000)
                return utc
            else:
                return 0

    def send_message(self, msg):
        self.ensure_connection_status()

        if self.callback and callable(self.callback.tx_started):
            self.callback.tx_started()

        if self.queue_message(msg) and self.attempt_connection():

            session_delay = 1
            session_attempts = 3

            while True:
                session_attempts = session_attempts - 1
                if session_attempts == 0:
                    break

                if self.attempt_session():
                    return True
                else:
                    time.sleep(session_delay)

        if self.callback and callable(self.callback.tx_failed):
            self.callback.tx_failed()

        return False

    def get_serial_identifier(self):
        self.ensure_connection_status()

        command = 'AT+GSN'

        self.s.write(command + '\r')

        if self.s.readline().strip() == command:
            response = self.s.readline().strip()

            self.s.readline().strip()  # BLANK
            self.s.readline().strip()  # OK

            return response

    # One-time initial setup function (Disables Flow Control)
    # This only needs to be called once, as is stored in non-volitile memory

    # Make sure you DISCONNECT RockBLOCK from power for a few minutes after this command has been issued...
    def setup(self):
        self.ensure_connection_status()

        # Disable Flow Control
        command = 'AT&K0'

        self.s.write(command + '\r')

        if self.s.readline().strip() == command and self.s.readline().strip() == 'OK':

            # Store Configuration into Profile0
            command = 'AT&W0'

            self.s.write(command + '\r')

            if self.s.readline().strip() == command and self.s.readline().strip() == 'OK':

                # Use Profile0 as default
                command = 'AT&Y0'

                self.s.write(command + '\r')

                if self.s.readline().strip() == command and self.s.readline().strip() == 'OK':

                    # Flush Memory
                    command = 'AT*F'

                    self.s.write(command + '\r')

                    if self.s.readline().strip() == command and self.s.readline().strip() == 'OK':
                        # self.close()

                        return True

        return False

    def close(self):
        if self.s:
            self.s.close()
            self.s = None

    @staticmethod
    def list_ports():

        if sys.platform.startswith('win'):

            ports = ['COM' + str(i + 1) for i in range(256)]

        elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):

            ports = glob.glob('/dev/tty[A-Za-z]*')

        elif sys.platform.startswith('darwin'):

            ports = glob.glob('/dev/tty.*')

        result = []

        for port in ports:
            try:
                s = serial.Serial(port)
                s.close()
                result.append(port)
            except (OSError, serial.SerialException):
                pass

        return result

    # Private Methods - Don't call these directly!
    def queue_message(self, msg):
        self.ensure_connection_status()

        if len(msg) > 340:
            LOGGER.warning('sendMessageWithBytes bytes should be <= 340 bytes') 

            return False

        # read_line_q.get(timeout=30)
        command = 'AT+SBDWB=' + str(len(msg))

        self.s.write(command + '\r')
        LOGGER.info('write: {} to read_line_q'.format(command))
        time.sleep(3)

        send_cmd = self.read_line_q.get(timeout=30)
        if send_cmd == command:

            rec_msg = self.read_line_q.get(timeout=30)
            if rec_msg == 'READY':

                checksum = 0

                for c in msg:
                    checksum = checksum + ord(c)

                self.s.write(str(msg))

                self.s.write(chr(checksum >> 8))
                self.s.write(chr(checksum & 0xFF))

                result = False

                res = self.read_line_q.get(timeout=30)
                if res == '0':
                    result = True

                self.read_line_q.get(timeout=30)

                return result

        return False

    def _configure_port(self):
        if self._enable_echo() and self._disable_flow_control() and self._disable_ring_alerts() and self.ping():
            return True
        else:
            return False

    def _enable_echo(self):
        self.ensure_connection_status()

        command = 'ATE1'

        self.s.write(command + '\r')

        response = self.s.readline().strip()

        if response == command or response == '':

            if self.s.readline().strip() == 'OK':
                return True

        return False

    def _disable_flow_control(self):
        self.ensure_connection_status()

        command = 'AT&K0'

        self.s.write(command + '\r')

        if self.s.readline().strip() == command:

            if self.s.readline().strip() == 'OK':
                return True

        return False

    def _disable_ring_alerts(self):
        self.ensure_connection_status()

        command = 'AT+SBDMTA=0'

        self.s.write(command + '\r')

        if self.s.readline().strip() == command:

            if self.s.readline().strip() == 'OK':
                return True

        return False

    def enable_ring_alerts(self):
        self.ensure_connection_status()

        command = 'AT+SBDMTA=1'
        self.s.write(command + '\r')
        self.s.readline()
        response = self.s.readline().strip()
        if response == 'OK':
            LOGGER.info('Enable ring succeeded')
            return True
        LOGGER.warning('Enable ring failed')
        return False

    def attempt_session(self):
        self.ensure_connection_status()

        session_attempts = 10

        while True:

            if session_attempts == 0:
                LOGGER.error(u'Retried 10 times, but failed')
                return False

            session_attempts = session_attempts - 1

            command = 'AT+SBDIX'

            self.s.write(command + '\r')
            LOGGER.info('write: {} to read_line_q'.format(command))
            time.sleep(3)

            send_cmd = self.read_line_q.get(timeout=30)

            if send_cmd == command:

                response = self.read_line_q.get(timeout=30)

                if response.find('+SBDIX:') >= 0:

                    self.read_line_q.get(timeout=30)

                    # +SBDIX:<MO status>,<MOMSN>,<MT status>,<MTMSN>,<MT length>,<MTqueued>
                    response = response.replace('+SBDIX: ', '')

                    parts = response.split(',')

                    mo_status = int(parts[0])
                    mo_msn = int(parts[1])
                    mt_status = int(parts[2])
                    mt_msn = int(parts[3])
                    mt_length = int(parts[4])
                    mt_queued = int(parts[5])

                    # Mobile Originated
                    if mo_status <= 4:
                        self._clear_mo_buffer()
                        if self.callback is not None and callable(self.callback.tx_success):
                            self.callback.tx_success(mo_msn)
                    else:
                        if self.callback is not None and callable(self.callback.tx_failed):
                            LOGGER.warning('mo_status: {}, cant tx.'.format(mo_status))
                            self.callback.tx_failed()

                    if mt_status == 1 and mt_length > 0:  # SBD message successfully received from the GSS.
                        self._process_mt_message(mt_msn)

                    # AUTOGET NEXT MESSAGE

                    if self.callback is not None and callable(self.callback.rx_message_queue):
                        self.callback.rx_message_queue(mt_queued)

                    # There are additional MT messages to queued to download
                    if mt_queued > 0 and self.autoSession:
                        LOGGER.info(u'mt_queue: {} > 0 , 继续递归处理!'.format(mt_queued))
                        self.attempt_session()

                    if mo_status <= 4:
                        return True

        return False

    def attempt_connection(self):
        self.ensure_connection_status()

        time_attempts = 20
        time_delay = 1

        signal_attempts = 30
        rescan_delay = 10
        signal_threshold = 2

        # Wait for valid Network Time
        while True:

            if time_attempts == 0:

                if self.callback is not None and callable(self.callback.signal_fail):
                    LOGGER.warning('network time is invalid')
                    self.callback.signal_fail()

                return False

            if self._is_network_time_valid():
                LOGGER.info('network time is valid')
                break

            time_attempts = time_attempts - 1

            time.sleep(time_delay)

        # Wait for acceptable signal strength
        while True:

            signal = self.request_signal_strength()
            LOGGER.info('signal: {}'.format(signal))

            if signal_attempts == 0 or signal < 0:

                LOGGER.warning('NO SIGNAL')

                if self.callback is not None and callable(self.callback.signal_fail):
                    self.callback.signal_fail()

                return False

            self.callback.signal_update(signal)

            if signal >= signal_threshold:

                if self.callback is not None and callable(self.callback.signal_pass):
                    self.callback.signal_pass()

                return True

            signal_attempts = signal_attempts - 1

            time.sleep(rescan_delay)

    def _process_mt_message(self, mt_msn):
        self.ensure_connection_status()

        self.s.write('AT+SBDRB\r')

        response = self.read_line_q.get(timeout=30)

        if response == 'OK':
            LOGGER.warning('No message content.. strange!')

            if self.callback is not None and callable(self.callback.rx_received):
                self.callback.rx_received(mt_msn, '')

        else:
            content = response[2:-2]
            self.read_line_q.get(timeout=30)  # 'ok'

            if self.callback is not None and callable(self.callback.rx_received):
                self.callback.rx_received(mt_msn, content)

    def _is_network_time_valid(self):
        self.ensure_connection_status()

        command = 'AT-MSSTM'

        self.s.write(command + '\r')
        LOGGER.info('write: {} to read_line_q'.format(command))
        time.sleep(3)

        if self.read_line_q.get(timeout=30) == command:  # Echo

            response = self.read_line_q.get(timeout=30)

            if response.startswith('-MSSTM'):  # -MSSTM: a5cb42ad / no network service

                self.read_line_q.get(timeout=30)

                if len(response) == 16:
                    return True

        return False

    def _clear_mo_buffer(self):
        self.ensure_connection_status()

        command = 'AT+SBDD0'

        self.s.write(command + '\r')
        LOGGER.info('write: {} to read_line_q'.format(command))
        time.sleep(3)
        send_cmd = self.read_line_q.get(timeout=30)

        if send_cmd == command:

            recv_msg = self.read_line_q.get(timeout=30)
            if recv_msg == '0':

                if self.read_line_q.get(timeout=30) == 'OK':
                    return True

        return False

    def ensure_connection_status(self):

        if self.s is None or not self.s.isOpen():
            raise RockBlockException('rockblock serial is None or is closed..')

    def register_network(self):
        LOGGER.info('start register_network')
        self.ensure_connection_status()

        command = 'AT+SBDREG'
        self.s.write(command + '\r')
        write_cmd = self.s.readline().strip()
        LOGGER.info('write_cmd: {}'.format(write_cmd))
        if write_cmd == command:
            response = self.s.readline().strip()
            LOGGER.info('register_network response: {}'.format(response))
            self.s.readline()  # BLANK
            self.s.readline()  # OK
            if int(response.split('+SBDREG:')[1].split(',')[0]) == 2:
                LOGGER.info('Register succeeded')
                return True
        LOGGER.warning('Register failed')    
        return False

    def ring(self):
        try:
            if self.sbd_q.empty():
                return False
            res = self.sbd_q.get_nowait()
            LOGGER.info('Response: ' + res)
            if res == 'SBDRING':
                LOGGER.info('Receive a msg')
                return True
        except Exception as e:
            LOGGER.exception(e)
            return False
