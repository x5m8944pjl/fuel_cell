# -*- coding: UTF-8 –*-
"""
author:zqguo
time:2019-11-05 09:32
use python3.6+
"""
import json
import os
import re
import subprocess
import time
from datetime import datetime

import psutil
from paramiko import SSHClient
from scp import SCPClient

SCH_TASKS_NAME = ['beidou', 'buoy_corr_lidar_data', 'buoy_rsync_lidar_data', 'buoy_rsync_motion_data']
FILE_MONITOR = {
    "lidar": "D:/lidar_raw_data/buoy1/LIDAR",
    "motion": "D:/lidar_raw_data/buoy1/Motion",
    "trimble": "D:/lidar_raw_data/buoy1/Trimble",
    "weather": "D:/lidar_raw_data/buoy1/Weather",
    "corr_file_dir": "D:/Titan/lidar-dev-local/sbd_upload/simplified"
}


def time_str_to_timestamp(date_str):
    try:
        t = datetime.strptime(date_str, '%Y-%m-%d').timetuple()
        res = int(time.mktime(t))
        return res
    except Exception as e:
        print(e)
        return 0


def timestamp_to_date(t):
    time_local = time.localtime(t)
    dt = time.strftime("%Y_%m_%d", time_local)
    return dt


# 获取文件大小
def get_file_size(filepath):
    fsize = os.path.getsize(filepath)
    return round(fsize / 1024, 2)


def get_file_modify_time(filepath):
    t = os.path.getmtime(filepath)
    return int(t)


def file_monitor():
    rv = {}
    for type_, fpath in FILE_MONITOR.items():
        date_list = set(
            ['{}-{}-{}'.format(str(f)[-14:-10:], str(f)[-9:-7:], str(f)[-6:-4:]) for f in os.listdir(fpath)])
        if type_ == "lidar":
            wind10_date_list = set(
                ['{}-{}-{}'.format(str(f)[-20:-16], str(f)[-14:-12], str(f)[-10:-8]) for f in os.listdir(fpath) if
                 'Wind10_' in str(f)])
            wind_date_list = set(
                ['{}-{}-{}'.format(str(f)[-20:-16], str(f)[-14:-12], str(f)[-10:-8]) for f in os.listdir(fpath) if
                 'Wind_' in str(f)])

            wind10_timestamp = [time_str_to_timestamp(date_) for date_ in wind10_date_list]
            wind10_max_timestamp = max(wind10_timestamp)
            print(wind10_max_timestamp)

            wind_timestamp = [time_str_to_timestamp(date_) for date_ in wind_date_list]
            wind_max_timestamp = max(wind_timestamp)
            print(wind_max_timestamp)

            win10_latest_filename = time.strftime('Wind10_791@Y%Y_M%m_D%d.ZPH.zip',
                                                  time.localtime(wind10_max_timestamp))
            print(win10_latest_filename)

            win_latest_filename = time.strftime('Wind_791@Y%Y_M%m_D%d.ZPH.zip',
                                                time.localtime(wind_max_timestamp))
            print(win_latest_filename)

            win10_latest_filepath = os.path.join(fpath, win10_latest_filename)
            win10_latest_filesize = get_file_size(win10_latest_filepath)
            win10_latest_modify_time = get_file_modify_time(win10_latest_filepath)
            rv.update({
                'win10': [win10_latest_modify_time, win10_latest_filesize]
            })

            win_latest_filepath = os.path.join(fpath, win_latest_filename)
            win_latest_filesize = get_file_size(win_latest_filepath)
            win_latest_modify_time = get_file_modify_time(win_latest_filepath)
            rv.update({
                'win': [win_latest_modify_time, win_latest_filesize]
            })

        elif type_ == "motion":
            timestamp = [time_str_to_timestamp(date_) for date_ in date_list]
            max_timestamp = max(timestamp)
            latest_date = timestamp_to_date(max_timestamp)
            latest_date_filename = 'COM1_{}.txt'.format(latest_date)
            latest_date_filepath = os.path.join(fpath, latest_date_filename)
        elif type_ == "trimble":
            timestamp = [time_str_to_timestamp(date_) for date_ in date_list]
            max_timestamp = max(timestamp)
            latest_date = timestamp_to_date(max_timestamp)
            latest_date_filename = 'PORT3001_{}.txt'.format(latest_date)
            latest_date_filepath = os.path.join(fpath, latest_date_filename)
        elif type_ == "weather":
            timestamp = [time_str_to_timestamp(date_) for date_ in date_list]
            max_timestamp = max(timestamp)
            latest_date = timestamp_to_date(max_timestamp)
            latest_date_filename = 'COM2_{}.txt'.format(latest_date)
            latest_date_filepath = os.path.join(fpath, latest_date_filename)
        elif type_ == "corr_file_dir":
            # 20191028.7z
            date_list = set(
                ['{}-{}-{}'.format(str(f)[:4], str(f)[4:6], str(f)[6:8]) for f in os.listdir(fpath) if '7z' in str(f)])
            timestamp = [time_str_to_timestamp(date_) for date_ in date_list]
            max_timestamp = max(timestamp)
            time_local = time.localtime(max_timestamp)
            latest_date = time.strftime("%Y%m%d", time_local)
            latest_date_filename = '{}.7z'.format(latest_date)
            latest_date_filepath = os.path.join(fpath, latest_date_filename)

        if type_ != 'lidar':
            latest_date_filesize = get_file_size(latest_date_filepath)
            latest_date_modify_time = get_file_modify_time(latest_date_filepath)
            rv.update({
                type_: [latest_date_modify_time, latest_date_filesize]
            })
    return rv


def mem_and_disk_monitor():
    mem_aval = round(psutil.virtual_memory().available / (1024 * 1024 * 1024), 2)
    c_free = round(psutil.disk_usage('C:').free / (1024 * 1024 * 1024), 2)
    d_free = round(psutil.disk_usage('D:').free / (1024 * 1024 * 1024), 2)
    return {
        'mem_aval': mem_aval,
        'c_free': c_free,
        'd_free': d_free
    }


def upload_file_to_datapc():
    ssh = SSHClient()
    ssh.load_system_host_keys()
    ssh.connect(hostname='192.168.0.15', username='root', password='windkraft0815')
    scp = SCPClient(ssh.get_transport())
    scp.put('monitor_data.json', recursive=True, remote_path='/root/deploy/fuel_cell_app')
    scp.close()


if __name__ == '__main__':
    output = {}
    for sch_task_name in SCH_TASKS_NAME:
        try:
            out_bytes = subprocess.check_output(
                ['schtasks', '/Query', '/TN', sch_task_name])
        except subprocess.CalledProcessError as e:
            out_bytes = e.output  # Output generated before error
            # code = e.returncode  # Return code
        res = out_bytes.decode('utf8')
        # print(res)
        # print(type(res))
        if sch_task_name == 'beidou':
            if 'Running' in res:
                output[sch_task_name] = 'Running'
        else:
            match = re.search(r'\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}', res)
            dtstr = match.group()
            ts = int(time.mktime(datetime.strptime(dtstr, "%Y-%m-%d %H:%M:%S").timetuple()))
            output[sch_task_name] = ts
    print(output)
    file_status = file_monitor()
    print(file_status)

    mem_and_disk_status = mem_and_disk_monitor()

    file_status.update(mem_and_disk_status)
    file_status.update(output)
    with open('monitor_data.json', 'w') as f:
        json.dump(file_status, f)
    upload_file_to_datapc()
