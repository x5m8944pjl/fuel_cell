# -*- coding: UTF-8 –*-

from comm.modbus import ModBus
from comm.rockblock import RockBlockProtocol, RockBlock
from log import get_logger
from utils import get_config

cfg = get_config('cfg.json')

LOGGER = get_logger('fuel_cell_app', 'logging.conf')


class CMSAndControl(RockBlockProtocol):
    RESTART_CMD = 'Code:47x'
    REQUEST_CURRENT_STATUS = 'Code:48x'
    REQUEST_CURRENT_ALARM = 'Code:49x'

    def __init__(self, conf, rbs, read_line_q, sbd_q):
        self.conf = conf
        self.rb = RockBlock(self, rbs, read_line_q, sbd_q)
        self.rbs = rbs
        self.data = ''
        self.read_line_q = read_line_q
        self.sbd_q = sbd_q

    def connected(self):
        LOGGER.info('rockBlockConnected')

    def tx_started(self):
        LOGGER.info(u'rockblock 传输开启!')

    def tx_failed(self):
        LOGGER.info('rockBlockTxFailed')

    def tx_success(self, mo_msn):
        LOGGER.info('rockBlockTxSuccess: {}'.format(mo_msn))

    def rx_started(self):
        LOGGER.info('rockBlockRxStarted')

    def rx_failed(self):
        LOGGER.info('rockBlockRxFailed')
        self.rb.send_message('Failed')

    def rx_received(self, mt_msn, data):
        LOGGER.info('rockBlockRxReceived: {} {}'.format(mt_msn, data))
        self.handle_cmd(mt_msn, data)

    def send(self, value):
        self.rb.send_message(str(value))

    def handle_cmd(self, mt_msn, data):
        if 'Code:' not in data:
            return
        
        data = data.strip()
        data = data[data.index('Code')::]
        _data = data.split('-')
        LOGGER.info(_data)
        code = _data[0]

        handler_fuels = {}
        if len(_data) == 1:
            handler_fuels.update(cfg['comm_configs'])
        elif len(_data) == 2:
            numbers = _data[1].split(',')
            for number in numbers:
                if number in cfg['comm_configs']:
                    handler_fuels[number] = cfg['comm_configs'][number]
                else:
                    self.rb.send_message('Mtmsn {}: invalid number {}'.format(mt_msn, number))

        for number, comm_conf in handler_fuels.items():
            try:
                temp = {number: comm_conf}
                if code == self.RESTART_CMD:
                    from cms import restart_process
                    restart_process(mt_msn, comm_conf, number)
                    continue

                if code == self.REQUEST_CURRENT_STATUS:
                    from cms import get_status_and_send
                    get_status_and_send(temp)
                    LOGGER.info('No:{} get_status_and_send success'.format(number))
                    continue

                if code == self.REQUEST_CURRENT_ALARM:
                    from cms import get_alarm_and_send
                    get_alarm_and_send(temp)
                    LOGGER.info('No:{} get_alarm_and_send success'.format(number))
                    continue

                for key, value in cfg['cmd_list'].iteritems():
                    if code == key:
                        modbus = ModBus(comm_conf)
                        modbus.write_int16s_data(value['addr'], value['value'])
                        LOGGER.info('No:{},mtmsn:{} Success'.format(number,mt_msn))
                        self.rb.send_message('No:{},: Success'.format(number))
                        modbus.disconnect()
                        continue
            except Exception as e:
                LOGGER.exception(e)
                self.rb.send_message('No:{}: Failed'.format(number))

    def rec(self):
        try:
            if self.rb.ring():
                self.rb.message_check()
        except Exception as e:
            LOGGER.exception(e)
