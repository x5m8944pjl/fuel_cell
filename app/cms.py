# -*- coding: UTF-8 –*-
import Queue
import threading
import time
from threading import Timer

import serial
from modbus_tk.exceptions import ModbusInvalidResponseError

from comm.modbus import ModBus
from control import CMSAndControl
from db import find_latest_alarm, insert_alarm, update_alarm_status
from log import get_logger
from monitor.system_monitor import monitor_data_pc, monitor_backup_pc
from utils import round_up, get_config

LOGGER = get_logger('fuel_cell_app', 'logging.conf')
restart_count = 0
delay_count = 0
previous_date = ''

sbd_q = Queue.Queue()
read_line_q = Queue.Queue()
task_q = Queue.Queue()

cms = None
cfg = get_config('cfg.json')


def init_rockblock_serial(conf):
    port = conf['port']
    baudrate = conf['baudrate']
    LOGGER.info('init rockblock serial')
    rv = serial.Serial(port, baudrate, timeout=30)
    LOGGER.info('init rockblock serial is success')
    return rv


def send_msg_use_rockblock(value):
    try:
        LOGGER.info('start send msg via rockblock.')
        cms.send(value)
        LOGGER.info('send msg：{} use rockblock is success.'.format(value))
    except Exception as e:
        LOGGER.exception(e)


def rev_commands_use_rockblock():
    try:
        LOGGER.info('start to receive msgs via rockblock')
        cms.rec()
    except Exception as e:
        LOGGER.exception(e)


def init_process(conf, s):
    try:
        do = ModBus(conf)
        r = 2 if s else 0
        do.write_int16s_data(1022, 1)
        time.sleep(3)
        do.write_int16s_data(1000, r)
        do.disconnect()
        return True
    except Exception as e:
        LOGGER.exception(e)
        return False


def extern_control(s):
    do = ModBus(cfg['comm_config'])
    r = True if s else False
    LOGGER.debug(do.write_int16s_data(1030, r))
    do.disconnect()


def restart_process(mt_msn, comm_conf, number):
    try:
        global restart_count, previous_date
        current_date = time.strftime('%Y-%m-%d', time.localtime())
        if current_date != previous_date:
            restart_count = 0
            previous_date = current_date
        restart_count += 1
        if restart_count <= cfg['scheduler']['restart_count']:
            LOGGER.info('Restart the fuel cell')
            modbus = ModBus(comm_conf)
            modbus.write_int16s_data(2000, 0xBEAF)
            msg = 'No:{} Mtmsn {}: Success'.format(number, mt_msn)
            send_msg_use_rockblock(msg)
            modbus.disconnect()
    except ModbusInvalidResponseError as e:
        LOGGER.exception(e)
        msg = 'No:{} Mtmsn {}: Fail'.format(number, mt_msn)
        send_msg_use_rockblock(msg)
    except Exception as e:
        LOGGER.exception(e)
        msg = 'No:{} Mtmsn {}: Fail'.format(number, mt_msn)
        send_msg_use_rockblock(msg)


def exception_process(status):
    try:
        global delay_count
        if status['Ubat'] < cfg['cell_config']['switch_on_voltage']:
            delay_count += 1
            if delay_count >= cfg['cell_config']['restart_delay_count']:
                delay_count = 0
                restart_process(cfg)
        else:
            delay_count = 0
    except Exception as e:
        LOGGER.exception(e)


def get_alarm_and_send(config):
    LOGGER.info('get alarm get Serial port')
    i = 1
    LOGGER.info('Alarm_status time: {}'.format(int(time.time())))
    comm_configs = config
    for number, comm_conf in comm_configs.items():
        while True:
            modbus = None
            try:
                LOGGER.info('Alarm_status try {}'.format(i))
                if i > 3:
                    LOGGER.warning('Alarm_status try 3 this fail, exit.')
                    break
                i += 1
                status = {}
                modbus = ModBus(comm_conf)
                for key, value in cfg['alarm_list'].iteritems():
                    LOGGER.debug(value)
                    if value['type'] == 'INT32':
                        status[value['abbr']] = modbus.read_int32s_data(value['addr'], 1)[0] * value['scale']
                    elif value['type'] == 'INT16':
                        status[value['abbr']] = modbus.read_int16s_data(value['addr'], 1)[0] * value['scale']
                    elif value['type'] == 'UINT16':
                        status[value['abbr']] = modbus.read_uint16s_data(value['addr'], 1)[0] * value['scale']
                    elif value['type'] == 'UINT32':
                        status[value['abbr']] = modbus.read_uint32s_data(value['addr'], 1)[0] * value['scale']
                    status[value['abbr']] = data_process(status[value['abbr']])
                    LOGGER.info(status[value['abbr']])
                    time.sleep(1)
                LOGGER.info(status)
                send_msg = 'F_A_{}:{},{},{}'.format(number, status['LastError'], status['Error'],
                                                    status['Warning'])
                LOGGER.info(send_msg)
                latest_alarm = find_latest_alarm()
                LOGGER.info('latest_alarm: {}'.format(latest_alarm))
                if status['LastError'] == 0 and status['Error'] == 0 and status['Warning'] == 0:
                    LOGGER.info(u'alarm is all 0')
                    if latest_alarm and latest_alarm['status'] == 1:
                        LOGGER.info(u'恢复')
                        update_alarm_status(latest_alarm['id'])
                        send_msg_use_rockblock(u'{} has been restored'.format(latest_alarm['msg']))
                else:
                    if latest_alarm is None:
                        insert_alarm(send_msg)
                        send_msg_use_rockblock(send_msg)
                    else:
                        latest_msg = latest_alarm['msg']
                        if latest_msg == send_msg and latest_alarm['status'] == 1:
                            LOGGER.warning(u'与上次报警一样，不推送。')

                        # if latest_msg == send_msg and latest_alarm['status'] == 2:
                        #     insert_alarm(send_msg)
                        #     send_msg_use_rockblock(send_msg)

                        if latest_msg != send_msg or (latest_msg == send_msg and latest_alarm['status'] == 2):
                            insert_alarm(send_msg)
                            send_msg_use_rockblock(send_msg)

                    # send_msg = 'fuel_alarm_test:0,0,0'
                    LOGGER.info('get fuel alarm status success .')
                    # Exception_process(cfg, status)
                break
            except Exception as e:
                LOGGER.exception(e)
            finally:
                modbus.disconnect()


def monitor_data_and_backup_pc(p1):
    status_servers, lidar_status, process_status, file_status, mem_and_dis_status = monitor_data_pc()
    status_for_backpc = monitor_backup_pc()

    LOGGER.info(status_servers)
    LOGGER.info(lidar_status)
    LOGGER.info(process_status)
    LOGGER.info(file_status)
    LOGGER.info(mem_and_dis_status)
    LOGGER.info(status_for_backpc)

    data_pc_send_msg = 'D_M:{},{},{},{},{},{},{},{},{},{};{},{},{},{},{},{};{},{};{},{};{},{};{},{};{},{};{},{}'.format(
        status_servers['Trimble'], status_servers['camera1'], status_servers['camera2'],
        status_servers['LIDAR'], status_servers['Motion'], status_servers['4G_router'],
        status_servers['WIFI_ampification'], status_servers['Back_PC2'],
        status_servers['Back_PC1'], status_servers['Pilot'],
        process_status['beaconCtr'], process_status['RecCOMV1.1'], process_status['fuel_cell_monitor'],
        process_status['ssh_tunnel_9100'], process_status['vsftpd'], process_status['filemon'],
        file_status['motion'][0], file_status['motion'][1],
        file_status['trimble'][0], file_status['trimble'][1],
        file_status['weather'][0], file_status['weather'][1],
        lidar_status['wind10'][0], lidar_status['wind10'][1],
        lidar_status['wind'][0], lidar_status['wind'][1],
        mem_and_dis_status['mem_available'], mem_and_dis_status['disk_free']
    )
    backup_pc_send_msg = 'B_M:{};{},{},{};{},{};{},{};{},{};{},{};{},{};{},{};{},{},{}'.format(
        1 if status_for_backpc['beidou_status'] == 'Running' else 0, status_for_backpc['buoy_rsnc_motion_next_time'],
        status_for_backpc['buoy_rsnc_lidar_next_time'], status_for_backpc['buoy_corr_data_next_time'],
        status_for_backpc['lastest_motion_ts'], status_for_backpc['lastest_motion_filesize'],
        status_for_backpc['lastest_trimble_ts'], status_for_backpc['lastest_trimble_filesize'],
        status_for_backpc['lastest_weather_ts'], status_for_backpc['lastest_weather_filesize'],
        status_for_backpc['lastest_corr_ts'], status_for_backpc['lastest_corr_filesize'],
        status_for_backpc['lastest_lidar10_ts'], status_for_backpc['lastest_lidar10_filesize'],
        status_for_backpc['lastest_lidar_ts'], status_for_backpc['lastest_lidar_filesize'],
        status_for_backpc['mem_aval'], status_for_backpc['c_free'], status_for_backpc['d_free']
    )
    LOGGER.info('monitor_data_and_backup_pc_msg'.format(data_pc_send_msg))
    send_msg_use_rockblock(data_pc_send_msg)
    send_msg_use_rockblock(backup_pc_send_msg)


def data_process(number):
    if isinstance(number, float):
        number = round_up(number)
    return number


def get_status_and_send(config):
    LOGGER.info('get status get Serial port')
    i = 1
    LOGGER.info('App_get_status time: {}'.format(int(time.time())))
    comm_configs = config
    for number, comm_conf in comm_configs.items():
        while True:
            try:
                LOGGER.info('App_get_status try {}'.format(i))
                if i > 3:
                    LOGGER.warning('App_get_status try 3 this fail, exit.')
                    break
                status = {}
                modbus = ModBus(comm_conf)
                LOGGER.info('do: {}'.format(modbus))
                for key, value in cfg['status_addr_list'].iteritems():
                    LOGGER.info(value)
                    if value['type'] == 'INT32':
                        status[value['abbr']] = modbus.read_int32s_data(value['addr'], 1)[0] * value['scale']
                    elif value['type'] == 'INT16':
                        status[value['abbr']] = modbus.read_int16s_data(value['addr'], 1)[0] * value['scale']
                    elif value['type'] == 'UINT16':
                        status[value['abbr']] = modbus.read_uint16s_data(value['addr'], 1)[0] * value['scale']
                    elif value['type'] == 'UINT32':
                        status[value['abbr']] = modbus.read_uint32s_data(value['addr'], 1)[0] * value['scale']
                    status[value['abbr']] = data_process(status[value['abbr']])
                    LOGGER.info(status[value['abbr']])
                    time.sleep(1)
                LOGGER.info(status)
                send_msg = 'F_S_{}:{},{},{};{},{};{},{};{},{};{},{}'.format(
                    number, status['Tst'], status['Tint'], status['Twt'],
                    status['Ubat'], status['Iaus'],
                    status['OnReason'], status['OffReason'],
                    status['Mode'], status['Status'],
                    status['Cart1'], status['Cart2']
                )
                # send_msg = 'fuel_status_test:26.762,25.968,26.135;25.215,-0.021;0,0;2,1;0.0,4294966.296,
                # 4294966.296,0.0,' \ '4294966.296,4294966.296 '
                LOGGER.info('send_msg: {}'.format(send_msg))
                LOGGER.info('get fuel status success .')
                modbus.disconnect()
                LOGGER.info('start send msg using rockblock..')
                send_msg_use_rockblock(send_msg)
                LOGGER.info('msg: {} using rockblock is success'.format(send_msg))
                break
            except Exception as e:
                LOGGER.info('get fuel status failed .')
                LOGGER.exception(e)
            finally:
                i += 1
                # exception_process(cfg, status)


def rec_commands(cfg):
    rev_commands_use_rockblock()


def exec_task():
    while True:
        try:
            LOGGER.info('Q size: ' + str(task_q.qsize()))
            if not task_q.empty():
                task = task_q.get()
                LOGGER.info('Start to exec ' + task[0].func_name)
                task[0](task[1])
                LOGGER.info('exec {} is success'.format(task[0].func_name))
        except Exception as e:
            LOGGER.exception(e)
        finally:
            time.sleep(3)


def readline_and_put_to_queue(rbs):
    while True:
        try:
            msg = rbs.readline().strip()
            if not msg:
                continue
            LOGGER.info('读取到rbs-msg: {}'.format(msg))
            if 'SBDRING' in msg:
                sbd_q.put('SBDRING')
            else:
                read_line_q.put(msg)
        except Exception as e:
            LOGGER.exception(e)


def write_msg(rbs):
    while True:
        rbs.write('AT+SBDRB\r')
        time.sleep(5)


def get_msg():
    while True:
        LOGGER.info('qsize: {}'.format(read_line_q.qsize()))
        msg = read_line_q.get(timeout=30)
        LOGGER.info('get msg: {}'.format(msg))


'''
def put_task_to_queue():
    get_status_interval = cfg['scheduler']['get_status_interval']
    get_alarm_status_interval = cfg['scheduler']['get_alarm_status_interval']
    commands_rev_interval = cfg['scheduler']['commands_rev_interval']
    last_time_get_status = int(time.time())
    last_time_get_alarm_status = int(time.time())
    last_time_commands_rev = int(time.time())
    while True:
        current_time = int(time.time())

        get_status_interval_ = current_time - last_time_get_status
        get_alarm_status_interval_ = current_time - last_time_get_alarm_status
        commands_rev_interval_ = current_time - last_time_commands_rev
        LOGGER.debug('get_status_interval_: {}, get_alarm_status_interval_: {} commands_rev_interval_: {}'.format(
            get_status_interval_, get_alarm_status_interval_, commands_rev_interval_)
        )
        if get_status_interval <= get_status_interval_:
            last_time_get_status = int(time.time())
            task_q.put(get_status_and_send)
            LOGGER.info('put: {} to task_q'.format(get_status_and_send.func_name))

        if get_alarm_status_interval <= get_alarm_status_interval_:
            last_time_get_alarm_status = int(time.time())
            task_q.put(get_alarm_and_send)
            LOGGER.info('put: {} to task_q'.format(get_alarm_and_send.func_name))
        if commands_rev_interval <= commands_rev_interval_:
            last_time_commands_rev = int(time.time())
            task_q.put(rec_commands)
            LOGGER.info('put: {} to task_q'.format(rec_commands.func_name))
'''


def put_get_status_to_queue(interval):
    task_q.put([get_status_and_send, cfg['comm_configs']])
    LOGGER.info('put {} to task_q'.format(get_status_and_send.func_name))
    t = Timer(interval, put_get_status_to_queue, (interval,))
    t.start()


def put_get_alarm_to_queue(interval):
    task_q.put([get_alarm_and_send, cfg['comm_configs']])
    LOGGER.info('put {} to task_q'.format(get_alarm_and_send.func_name))
    t = Timer(interval, put_get_alarm_to_queue, (interval,))
    t.start()


def put_rec_cmd_to_queue(interval):
    task_q.put([rec_commands, None])
    LOGGER.info('put {} to task_q'.format(rec_commands.func_name))
    t = Timer(interval, put_rec_cmd_to_queue, (interval,))
    t.start()


def put_monitor_data_and_backpc_to_queue(interval):
    task_q.put([monitor_data_and_backup_pc, None])
    LOGGER.info('put {} to task_q'.format(monitor_data_and_backup_pc.func_name))
    t = Timer(interval, put_monitor_data_and_backpc_to_queue, (interval,))
    t.start()


def register_network(rbs):
    rbs.flushInput()
    while True:
        if cms.rb.register_network() and cms.rb.enable_ring_alerts():
            break
        time.sleep(60)
    rbs.flushInput()


def init_devices():
    comm_configs = cfg['comm_configs']
    for number, comm_config in comm_configs.items():
        if init_process(comm_config, False):
            LOGGER.info("No:{} Init succeeded".format(number))
        else:
            LOGGER.info("No:{} Init Faied".format(number))


def start():
    # 改用多线程方式

    rbs = init_rockblock_serial(cfg['iridium_config'])

    global cms

    cms = CMSAndControl(cfg['iridium_config'], rbs, read_line_q, sbd_q)

    init_devices()

    register_network_thread = threading.Thread(target=register_network, args=(rbs,))
    register_network_thread.start()
    register_network_thread.join()

    read_line_thread = threading.Thread(target=readline_and_put_to_queue, args=(rbs,))
    read_line_thread.start()

    # put_task_to_queue_thread = threading.Thread(target=put_task_to_queue, args=())
    # put_task_to_queue_thread.start()
    get_status_interval = cfg['scheduler']['get_status_interval']
    get_alarm_status_interval = cfg['scheduler']['get_alarm_status_interval']
    commands_rev_interval = cfg['scheduler']['commands_rev_interval']
    monitor_data_and_backup_interval = cfg['scheduler']['monitor_data_and_backup_interval']

    put_get_status_to_queue(get_status_interval)
    put_get_alarm_to_queue(get_alarm_status_interval)
    put_rec_cmd_to_queue(commands_rev_interval)
    put_monitor_data_and_backpc_to_queue(monitor_data_and_backup_interval)

    exec_task_thread = threading.Thread(target=exec_task, args=())
    exec_task_thread.start()
