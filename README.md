fuel_status:[Stack_temperature],[Internal_temperature],[Heat_exchanger_temperature]:[Battery_voltage],[Output_current]:[Last_switch_on_reason],[Last_switch_off_reason]:[Operating_mode],[Operating_state]:[Fill_level_cartridge_port#1],[Fill_level_cartridge_port#2]

fuel_alarm:[Last_occurred_error],[Current_error],[Warnings]

D_M:[Trimble],[camera1],[camera2],[LIDAR],[Motion],[4G_router],[WIFI_ampification],[Back_PC2],[Back_PC1],[Pilot];
[beaconCtr],[RecCOMV1.1],[fuel_cell_monitor],[ssh_tunnel_9100],[vsftpd],[filemon];[motion_file_latest_ts],
[motion_file_latest_size];[trimble_file_latest_ts],[trimble_file_latest_size];[weather_file_latest_ts],
[weather_file_latest_size];[lastest_lidar10_ts],[lastest_lidar10_filesize];[lastest_lidar_ts],[lastest_lidar_filesize];
[mem_available],[disk_free]

B_M:[beidou_status];[buoy_rsnc_motion_next_time],[buoy_rsnc_lidar_next_time],[buoy_corr_data_next_time];
[lastest_motion_ts],[lastest_motion_filesize];[lastest_trimble_ts],[lastest_trimble_filesize];[lastest_weather_ts],
[lastest_weather_filesize];[lastest_corr_ts],[lastest_corr_filesize];[lastest_lidar10_ts],[lastest_lidar10_filesize];
[lastest_lidar_ts],[lastest_lidar_filesize];[mem_aval],[c_free],[d_free]