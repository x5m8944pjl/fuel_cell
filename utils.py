# -*- coding: UTF-8 –*-
import os

import simplejson as json

from log import get_logger

LOGGER = get_logger('fuel_cell_app', 'logging.conf')


def round_up(value):
    return round(value * 10) / 10.0


def get_config(path):
    if not os.path.isfile(path):
        LOGGER.error('Wrong Configuration file!')
    with open(path, 'rb') as f:
        data = json.load(f)
    return data
